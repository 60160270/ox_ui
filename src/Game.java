
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Game {

    private Player X;
    private Player O;
    private Board board;
    private Player winner;

    Game() {
        O = new Player('O');
        X = new Player('X');
        board = new Board(X, O);
    }
    /*void play() {
     showWelcome();
     while(true) {			
     while(true) {
     showTable();
     if(board.isFinish()) {
     showResult();
     showStat();
     break;
     }
     showTurn();
     input();								
     }
     showBye();
     }
     }*/

    void showWelcome() {
        board.getWelcome();
    }

    /*void showTable() {
     char[][] table = board.getTable();
     System.out.println("  1 2 3");
     for (int i = 0; i < 3; i++) {
     System.out.print(i + 1 + "|");
     for (int j = 0; j < 3; j++) {
     if (table[i][j] != '\u0000') {
     System.out.print(table[i][j]);
     } else {
     System.out.print(" ");
     }
     System.out.print("|");
     }
     System.out.println();
     }
     }*/
    Player showTurn() {
        return board.getCurrentPlayer();
    }

    boolean input(int row, int column) {
        try {
            if (board.setTable(row, column)); 
            else {
                if (!(row >= 0 && row < 3 && column >= 0 && column < 3)) {
                    JOptionPane.showMessageDialog(null,"Row and Column must be number 1 - 3");
                    //System.out.println("Row and Column must be number 1 - 3");
                    return false;
                } else {
                    JOptionPane.showMessageDialog(null,"Row " + (row + 1) + " and Column " + (column + 1) + " can't choose again");
                    //System.out.println("Row " + (row + 1) + " and Column " + (column + 1) + " can't choose again");
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println("Row and Column must be number");
            return false;
        }
        return true;
    }

    boolean showResult() {
        if (board.isFinish()) {
            winner = board.getWinner();
            newGame();
            return true;
        }
        return false;
    }

    char getWinner() {
        return this.winner.getName();
    }

    void showBye() {
        System.out.println("Thank you for playing game");
    }

    void showStat() {
        System.out.println("Stat X : WIN / DRAW / LOSE : " + X.getWin() + " / " + X.getDraw() + " / " + X.getLose());
        System.out.println("Stat O : WIN / DRAW / LOSE : " + O.getWin() + " / " + O.getDraw() + " / " + O.getLose());
    }

    void newGame() {
        board.clearTable();
    }
}
