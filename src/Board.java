
public class Board {

    static char table[][] = new char[3][3];
    private Player X;
    private Player O;
    private Player winner;
    private int turnCount;
    private Player current;
    private String welcome = "Welcome to XO game";

    Board(Player X, Player O) {
        this.X = X;
        this.O = O;
        this.current = X;
        this.winner = null;
        turnCount = 0;
    }

    boolean isFinish() {
        for (int i = 0; i < 3; i++) {
            if ((table[i][0] == table[i][1] && table[i][0] == table[i][2]) && table[i][i] != '\u0000') {
                winner = current;
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == table[1][i] && table[0][i] == table[2][i] && table[0][i] != '\u0000') {
                winner = current;
                return true;
            }
        }
        if (table[0][0] == table[1][1] && table[0][0] == table[2][2] && table[0][0] != '\u0000') {
            winner = current;
            return true;
        } else if (table[0][2] == table[1][1] && table[0][2] == table[2][0] && table[0][2] != '\u0000') {
            winner = current;
            return true;
        }
        if (turnCount == 9) {
            //System.out.println("DRAW");
            turnCount = 0;
            clearTable();
            return true;
        }
        return false;
    }

    char[][] getTable() {
        return table;
    }

    Player getCurrentPlayer() {
        return current;
    }

    boolean setTable(int row, int column) {
        if (!(row >= 0 && row < 3 && column >= 0 && column < 3)) {
            //System.out.println("Row and Column must be number 1 - 3");
            return false;
        } else if (table[row][column] == '\u0000') {
            table[row][column] = getCurrentPlayer().getName();

            if (isFinish()) ; else {
                turnCount++;
                switchTurn();
            }

            return true;
        } else if (table[row][column] != '\u0000') {
            //System.out.println("Row "+row+" and Column "+column+" can't choose again");
            return false;
        } else {
            //System.out.println("Row and Column must be number");
            return false;
        }
    }

    void switchTurn() {
        if (getCurrentPlayer().getName() == 'X') {
            current = O;
        } else {
            current = X;
        }
    }

    void getWelcome() {
        System.out.println(welcome);
    }

    Player getWinner() {
        if (winner == X) {
            X.addWin();
            O.addLose();
        } else if (winner == O) {
            O.addWin();
            X.addLose();
        } else {
            X.addDraw();
            O.addDraw();
        }
        return winner;
    }

    void clearTable() {
        //table[i][j]!='\u0000'
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '\u0000';
            }
        }
        this.current = X;
        this.winner = null;
        turnCount = 0;
        
    }
}
